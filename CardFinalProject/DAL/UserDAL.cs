﻿using BO;
using DAL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace DAL
{
    public class UserDAL
    {
        private Entities.Entity UserDBContext;
    
        public UserDAL()
        {
            UserDBContext = new Entities.Entity();
            
        }

        public bool AddUser(UserBO user)
        {
            Users User = new Users() { Email = user.Email, Password = MD5Hash(user.Password) };

           
            var ExistingUser = UserDBContext.Users
                        .Where(b => b.Email == user.Email)
                        .FirstOrDefault();

            if (ExistingUser == null)
            {

                UserDBContext.Users.Add(User);


                int lastProductId = UserDBContext.Users.Max(item => item.Id);
                lastProductId++;
                UserDBContext.UserRoles.Add(new UserRoles {  Role="User"});
                int result = UserDBContext.SaveChanges();


                if (result > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            return false;

        }

        public UserBO LoginUser(UserBO user)
        {
           
            string hashedPassword = MD5Hash(user.Password);
            var ExistingUser = UserDBContext.Users
                        .Where(b => b.Email == user.Email && b.Password == hashedPassword)
                        .FirstOrDefault();

          
            if (ExistingUser == null)
            {
                return null;
            }
            else
            {
                user.Password = "";//create view model for user PL 
                user.Role = GetRolesForUser(ExistingUser.Id);
                return user;
            }
            
        }

        public string[] GetRolesForUser(int userId)
        {

            return UserDBContext.UserRoles.Where(i => i.UserId == userId).Select(i => i.Role).ToArray();
        }



        public static string MD5Hash(string input)
        {
            StringBuilder hash = new StringBuilder();
            MD5CryptoServiceProvider md5provider = new MD5CryptoServiceProvider();
            byte[] bytes = md5provider.ComputeHash(new UTF8Encoding().GetBytes(input));

            for (int i = 0; i < bytes.Length; i++)
            {
                hash.Append(bytes[i].ToString("x2"));
            }
            return hash.ToString();
        }

        private void CreateSampleuser()
        {
            Users User = new Users() { Email = "kopetsky.yaroslav@gmail.com", Password = MD5Hash("123") };


            var ExistingUser = UserDBContext.Users
                        .Where(b => b.Email == " kopetsky.yaroslav@gmail.com")
                        .FirstOrDefault();

            if (ExistingUser == null)
            {

                UserDBContext.Users.Add(User);


                int lastProductId = UserDBContext.Users.Max(item => item.Id);
                lastProductId++;
                UserDBContext.UserRoles.Add(new UserRoles { Role = "Admin", UserId = lastProductId });
                int result = UserDBContext.SaveChanges();
            }
        }
    }
}
