﻿using BO;
using DAL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace DAL
{
    public class DataDAL
    {
        private Entities.Entity DataDbContext;
    
        public DataDAL()
        {
            DataDbContext = new Entities.Entity();
           
        }

        public bool AddData(DataBO dataToAdd)
        {
            Data DataToAdd = new Data() { Author = dataToAdd.Author, AddedAt = DateTime.Now, PublishedYear = dataToAdd.PublishedYear, Title = dataToAdd.Title, UserId = dataToAdd.UserId, UserName = dataToAdd.Username };

            DataDbContext.Data.Add(DataToAdd);
            int result = DataDbContext.SaveChanges();

            if (result > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public List<DataBO> GetData()
        {
            List<Data> data = DataDbContext.Data.ToList();
            List<DataBO> dataBO = new List<DataBO>();

            foreach(Data d in data)
            {
                dataBO.Add(new DataBO() { Id = d.Id, AddedAt = d.AddedAt, Author = d.Author, PublishedYear = d.PublishedYear, Title = d.Title, UserId = d.UserId, Username = d.UserName });
            }

            return dataBO;
        }

        public List<DataBO> GetData(string Username)
        {
            List<Data> data = DataDbContext.Data.Where(i=> i.UserName == Username).ToList();
            List<DataBO> dataBO = new List<DataBO>();

            foreach (Data d in data)
            {
                dataBO.Add(new DataBO() { Id = d.Id, AddedAt = d.AddedAt, Author = d.Author, PublishedYear = d.PublishedYear, Title = d.Title, UserId = d.UserId, Username = d.UserName });
            }

            return dataBO;
        }


        public DataBO GetDetails(int id)
        {
            Data d = DataDbContext.Data.Where(i => i.Id == id).FirstOrDefault();
            DataBO dataBO = new DataBO();
            if (d != null)
            {
               dataBO = new DataBO() { Id = d.Id, AddedAt = d.AddedAt, Author = d.Author, PublishedYear = d.PublishedYear, Title = d.Title, UserId = d.UserId, Username = d.UserName };


            }


            dataBO.RelatedObjects = GetRelatedObjects(4);
           

            


            return dataBO;
        }

        public List<DataBO> GetRelatedObjects(int count)
        {
            List<DataBO> RelatedObjectsBO = new List<DataBO>();
            var RelatedObj = DataDbContext.Data.OrderByDescending(i => i.Id).Take(count).ToList();
            foreach (Data data in RelatedObj)
            {
                RelatedObjectsBO.Add(new DataBO() { Id = data.Id, AddedAt = data.AddedAt, Author = data.Author, PublishedYear = data.PublishedYear, Title = data.Title, UserId = data.UserId, Username = data.UserName });

            }
            return RelatedObjectsBO;
        }


        public bool DeleteData(int Id)
        {
           Data  data = DataDbContext.Data.Where(i => i.Id == Id).FirstOrDefault();
            DataDbContext.Data.Remove(data);
            int result = DataDbContext.SaveChanges();
            if(result > 0)
            {
                return true;
            }

            return false;
        }
           


    }
}
