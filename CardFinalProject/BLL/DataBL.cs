﻿using BO;
using DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL
{
    public class DataBL
    {
        public bool SubmitData(DataBO dataToSubmit)
        {
            return new DataDAL().AddData(dataToSubmit);
        }
        
        public List<DataBO> GetData()
        {
            return new DataDAL().GetData();
        }

        public List<DataBO> GetData(string UserName)
        {
            return new DataDAL().GetData(UserName);
        }

        public DataBO GetDetails(int id)
        {
            return new DataDAL().GetDetails(id);
        }

        public List<DataBO> GetRelatedObjects(int count)
        {
            return new DataDAL().GetRelatedObjects(count);
        }

        public bool DeleteData(int id)
        {
            return new DataDAL().DeleteData(id);
        }
    }
}
