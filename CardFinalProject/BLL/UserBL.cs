﻿using BO;
using DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL
{
    public class UserBL
    {
        public bool AddUser(UserBO User)
        {
            
            return new UserDAL().AddUser(User);

        }
        public UserBO LoginUser(UserBO User)
        {
            return new UserDAL().LoginUser(User);
        }
        public string[] GetRolesForUser(int id)
        {
            return new UserDAL().GetRolesForUser(id);
        }
    }
}
