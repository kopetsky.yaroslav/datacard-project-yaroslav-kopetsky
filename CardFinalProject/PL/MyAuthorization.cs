﻿using BO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Filters;

namespace PL
{



    public class RoleAuthorizeAttribute : AuthorizeAttribute
    {
        private readonly string[] allowedroles;
        private WebRoleProvider RoleManager = new WebRoleProvider();
        public RoleAuthorizeAttribute(params string[] roles)
        {
            this.allowedroles = roles;
        }


        public override void OnAuthorization(AuthorizationContext filterContext)
        {
         

            UserBO User = (UserBO)HttpContext.Current.Session["User"];

            if (User != null)
            {
                //string []UserRoles = RoleManager.GetRolesForUser(filterContext.HttpContext.User.Identity.Name);

                bool isInRole = false;

                foreach (string Role in User.Role)
                {
                    if (allowedroles.Contains(Role))
                    {
                        isInRole = true;
                    }
                }



                if (!isInRole)
                {
                    filterContext.Result = new RedirectResult("~/User/TryLogin");
                }
                else
                {

                }
            }
            else
            {
                filterContext.Result = new RedirectResult("~/User/TryLogin");
            }



        }
    }
}

