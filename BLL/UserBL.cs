﻿using BO;
using DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL
{
    public class UserBL
    {
        /// <summary>
        /// Register User
        /// </summary>
        /// <param name="User">User Object</param>
        /// <returns></returns>
        public bool AddUser(UserBO User)
        {
            
            return new UserDAL().AddUser(User);

        }

        /// <summary>
        /// Deletes User
        /// </summary>
        /// <param name="Id">User Id</param>
        /// <returns></returns>
        public bool DeleteUser(int Id)
        {

            return new UserDAL().DeleteUser(Id);

        }

        /// <summary>
        /// Logins User With Credentials
        /// </summary>
        /// <param name="User">User Object</param>
        /// <returns></returns>
        public UserBO LoginUser(UserBO User)
        {
            return new UserDAL().LoginUser(User);
        }

        /// <summary>
        /// Returns array of Roles for User
        /// </summary>
        /// <param name="id">User Id</param>
        /// <returns></returns>
        public string[] GetRolesForUser(int id)
        {
            return new UserDAL().GetRolesForUser(id);
        }

        /// <summary>
        /// Returns List Of Users
        /// </summary>
        /// <returns></returns>
        public List<UserBO> GetUserList()
        {
            return new UserDAL().GetUserList();
        }

        /// <summary>
        /// Returns true if Email is in Valid Form
        /// </summary>
        /// <param name="Email">Email Address</param>
        /// <returns></returns>
        public bool IsValidEmail(string Email)
        {
            return new UserDAL().IsValidEmail(Email);
        }
        
    }
}
