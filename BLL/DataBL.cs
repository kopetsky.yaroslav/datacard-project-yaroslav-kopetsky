﻿using BO;
using DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL
{
    public class DataBL
    {
        /// <summary>
        /// Submits Data To DB
        /// </summary>
        /// <param name="dataToSubmit">Data BO</param>
        /// <returns></returns>

        public bool SubmitData(DataBO dataToSubmit)
        {
            return new DataDAL().AddData(dataToSubmit);
        }
        
        /// <summary>
        /// Returns List of Data from DB
        /// </summary>
        /// <returns></returns>
        public List<DataBO> GetData()
        {
            return new DataDAL().GetData();
        }

        /// <summary>
        /// Returns Added By User by Username
        /// </summary>
        /// <param name="UserName">Username</param>
        /// <returns></returns>
        public List<DataBO> GetData(string UserName)
        {
            return new DataDAL().GetData(UserName);
        }

        /// <summary>
        /// Returns Data Object by Id
        /// </summary>
        /// <param name="id">Data Object Id</param>
        /// <returns></returns>
        public DataBO GetDetails(int id)
        {
            return new DataDAL().GetDetails(id);
        }

        /// <summary>
        /// Returns a List of Data Objects
        /// </summary>
        /// <param name="count">Objects count</param>
        /// <returns></returns>
        public List<DataBO> GetRelatedObjects(int count)
        {
            return new DataDAL().GetRelatedObjects(count);
        }

        /// <summary>
        /// Deletes Data
        /// </summary>
        /// <param name="id">Object id</param>
        /// <returns></returns>
        public bool DeleteData(int id)
        {
            return new DataDAL().DeleteData(id);
        }

        /// <summary>
        /// Updates Data Object
        /// </summary>
        /// <param name="dataToSubmit">DataObject</param>
        /// <returns></returns>
        public bool UpdateData(DataBO dataToSubmit)
        {
            return new DataDAL().UpdateData(dataToSubmit);
        }
    }
}
