﻿using BO;
using DAL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace DAL
{
    public class UserDAL
    {
        private NewDataBaseEntities UserDBContext;
    
        public UserDAL()
        {
            UserDBContext = new NewDataBaseEntities();
            
        }


        /// <summary>
        /// Deletes User from Db by Id
        /// </summary>
        /// <param name="Id">Id</param>
        /// <returns></returns>
        public bool DeleteUser(int Id)
        {
            Users UserToDelete = UserDBContext.Users.Where(i => i.Id == Id).First();
            UserDBContext.Users.Remove(UserToDelete);
            int result = UserDBContext.SaveChanges();

            if(result > 1)
            {
                return true;

            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Add User To Db
        /// </summary>
        /// <param name="user">User Object</param>
        /// <returns></returns>
        public bool AddUser(UserBO user)
        {
            Users User = new Users() { Email = user.Email, Password = MD5Hash(user.Password) };

           
            var ExistingUser = UserDBContext.Users
                        .Where(b => b.Email == user.Email)
                        .FirstOrDefault();

            if (ExistingUser == null)
            {

                UserDBContext.Users.Add(User);
                UserDBContext.SaveChanges();

                int lastUserId = UserDBContext.Users.Where(i=>i.Email == User.Email).First().Id;

                UserDBContext.UserRoles.Add(new UserRoles {  Role="User", UserId = lastUserId});
                int result = UserDBContext.SaveChanges();


                if (result > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            return false;

        }

        /// <summary>
        /// Logins User
        /// </summary>
        /// <param name="user">User Object</param>
        /// <returns></returns>
        public UserBO LoginUser(UserBO user)
        {
           
            string hashedPassword = MD5Hash(user.Password);
            var ExistingUser = UserDBContext.Users
                        .Where(b => b.Email == user.Email && b.Password == hashedPassword)
                        .FirstOrDefault();

            



            if (ExistingUser == null)
            {
                return null;
            }
            else
            {
                user.Password = "";
                user.Role = GetRolesForUser(ExistingUser.Id);
                return user;
            }
            
        }

        public string[] GetRolesForUser(int userId)
        {

            return UserDBContext.UserRoles.Where(i => i.UserId == userId).Select(i => i.Role).ToArray();
        }

        /// <summary>
        /// Returns List Of Users
        /// </summary>
        /// <returns>List Of Users</returns>
        public List<UserBO> GetUserList()
        {
            var User = UserDBContext.Users.ToList();
            List<UserBO> usersToReturn = new List<UserBO>();
            foreach (Users user in User)
            {
                usersToReturn.Add(new UserBO() { Id = user.Id, Email = user.Email,Role= GetRolesForUser(user.Id) });
            }

            return usersToReturn;
        }

        /// <summary>
        /// Hashes Passwords to Md5 Hash
        /// </summary>
        /// <param name="input">Password String</param>
        /// <returns>Hashed string</returns>
        public static string MD5Hash(string input)
        {
            StringBuilder hash = new StringBuilder();
            MD5CryptoServiceProvider md5provider = new MD5CryptoServiceProvider();
            byte[] bytes = md5provider.ComputeHash(new UTF8Encoding().GetBytes(input));

            for (int i = 0; i < bytes.Length; i++)
            {
                hash.Append(bytes[i].ToString("x2"));
            }
            return hash.ToString();
        }

        
        /// <summary>
        /// Validates Email
        /// </summary>
        /// <param name="email">Email String</param>
        /// <returns>True/False Status</returns>
        public bool IsValidEmail(string email)
        {
            try
            {
                var addr = new System.Net.Mail.MailAddress(email);
                return addr.Address == email;
            }
            catch
            {
                return false;
            }
        }
    }
}
