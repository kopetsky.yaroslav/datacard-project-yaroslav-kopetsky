﻿using BO;
using DAL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace DAL
{
    public class DataDAL
    {
        private NewDataBaseEntities DataDbContext;

        public DataDAL()
        {
            DataDbContext = new NewDataBaseEntities();

        }

        /// <summary>
        /// Adds new data to Db. Returns true/false status
        /// </summary>
        /// <param name="dataToAdd">Data Object</param>
        /// <returns></returns>
        public bool AddData(DataBO dataToAdd)
        {
            Data DataToAdd = new Data() { Author = dataToAdd.Author, AddedAt = DateTime.Now, PublishedYear = dataToAdd.PublishedYear, Title = dataToAdd.Title, UserId = dataToAdd.UserId, UserName = dataToAdd.Username, RawText = dataToAdd.RawText };

            DataDbContext.Data.Add(DataToAdd);
            int result = DataDbContext.SaveChanges();

            if (result > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Returns List Of Data Objects
        /// </summary>
        /// <returns></returns>
        public List<DataBO> GetData()
        {
            List<Data> data = DataDbContext.Data.ToList();
            List<DataBO> dataBO = new List<DataBO>();

            foreach (Data d in data)
            {
                dataBO.Add(new DataBO() { Id = d.Id, AddedAt = d.AddedAt, Author = d.Author, PublishedYear = d.PublishedYear, Title = d.Title, UserId = d.UserId, Username = d.UserName });
            }

            return dataBO;
        }

        /// <summary>
        /// Returns List of Data For User
        /// </summary>
        /// <param name="Username">Username</param>
        /// <returns></returns>
        public List<DataBO> GetData(string Username)
        {
            List<Data> data = DataDbContext.Data.Where(i => i.UserName == Username).ToList();
            List<DataBO> dataBO = new List<DataBO>();

            foreach (Data d in data)
            {
                dataBO.Add(new DataBO() { Id = d.Id, AddedAt = d.AddedAt, Author = d.Author, PublishedYear = d.PublishedYear, Title = d.Title, UserId = d.UserId, Username = d.UserName });
            }

            return dataBO;
        }

        /// <summary>
        /// Returns Data Object by Id
        /// </summary>
        /// <param name="id">Id</param>
        /// <returns></returns>
        public DataBO GetDetails(int id)
        {
            Data d = DataDbContext.Data.Where(i => i.Id == id).FirstOrDefault();
            DataBO dataBO = new DataBO();
            if (d != null)
            {
                dataBO = new DataBO() { Id = d.Id, AddedAt = d.AddedAt, Author = d.Author, PublishedYear = d.PublishedYear, Title = d.Title, UserId = d.UserId, Username = d.UserName, RawText = d.RawText };


            }


            dataBO.RelatedObjects = GetRelatedObjects(4);





            return dataBO;
        }

        /// <summary>
        /// Returns list of Data
        /// </summary>
        /// <param name="count">Objects Count</param>
        /// <returns></returns>
        public List<DataBO> GetRelatedObjects(int count)
        {
            List<DataBO> RelatedObjectsBO = new List<DataBO>();
            var RelatedObj = DataDbContext.Data.OrderByDescending(i => i.Id).Take(count).ToList();
            foreach (Data data in RelatedObj)
            {
                RelatedObjectsBO.Add(new DataBO() { Id = data.Id, AddedAt = data.AddedAt, Author = data.Author, PublishedYear = data.PublishedYear, Title = data.Title, UserId = data.UserId, Username = data.UserName });

            }
            return RelatedObjectsBO;
        }

        /// <summary>
        /// Deletes Data Object from Db
        /// </summary>
        /// <param name="Id">Object Id</param>
        /// <returns></returns>
        public bool DeleteData(int Id)
        {
            Data data = DataDbContext.Data.Where(i => i.Id == Id).FirstOrDefault();
            DataDbContext.Data.Remove(data);
            int result = DataDbContext.SaveChanges();
            if (result > 0)
            {
                return true;
            }

            return false;
        }

        /// <summary>
        /// Updates Data Object 
        /// </summary>
        /// <param name="dataToUpdate">Data Object</param>
        /// <returns></returns>
        public bool UpdateData(DataBO dataToUpdate)
        {
            Data data = DataDbContext.Data.Where(i => i.Id == dataToUpdate.Id).FirstOrDefault();

            data.PublishedYear = dataToUpdate.PublishedYear;
            data.Title = dataToUpdate.Title;
            data.Author = dataToUpdate.Author;
            data.RawText = dataToUpdate.RawText;
            DataDbContext.Entry(data).State = System.Data.Entity.EntityState.Modified;
            int result = DataDbContext.SaveChanges();

            if (result > 0)
            {
                return true;
            }

            return false;


        }
    }
}
