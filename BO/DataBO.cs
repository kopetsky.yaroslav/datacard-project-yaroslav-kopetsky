﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BO
{
    public class DataBO
    {
 
        public int Id { get; set; }
        public int UserId { get; set; }
        public string Title { get; set; }
        public string Author { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd-MM-yyyy}", ApplyFormatInEditMode = true)]
        public DateTime PublishedYear { get; set; } 

        public DateTime AddedAt { get; set; }
        public string Username { get; set; }

        public List<DataBO> RelatedObjects { get; set; }
        public string RawText { get; set; }
    }
}
