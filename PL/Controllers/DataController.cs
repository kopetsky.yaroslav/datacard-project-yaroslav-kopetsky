﻿using BLL;
using BO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PL.Controllers
{
    public class DataController : Controller
    {
        // GET: Data
        [RoleAuthorize("User", "Admin")]
        public ActionResult Index()
        {
            DataBL DataLogic = new DataBL();
            ViewBag.Cards = DataLogic.GetData();

            return View(ViewBag.Cards);
        }

        [RoleAuthorize("User", "Admin")]
        public ActionResult ViewText(int id)
        {
            DataBL DataLogic = new DataBL();
            ViewBag.Text = DataLogic.GetDetails(id);

            return View(ViewBag.Text);
        }



        [RoleAuthorize("Admin")]
        public ActionResult DataTable()
        {
            DataBL DataLogic = new DataBL();

            return View(DataLogic.GetData());
        }

        [RoleAuthorize("User", "Admin")]
        public ActionResult MyData()
        {
            DataBL DataLogic = new DataBL();
            UserBO User = (UserBO)HttpContext.Session["User"];
            ViewBag.Cards = DataLogic.GetData(User.Email);

            return View(ViewBag.Cards);
        }


        [RoleAuthorize("User", "Admin")]
        public ActionResult Details(int id)
        {
            DataBL DataLogic = new DataBL();
            ViewBag.Data = DataLogic.GetDetails(id);

            return View(ViewBag.Data);
        }

        [RoleAuthorize("User", "Admin")]
        public ActionResult AddCard()
        {


            return View();
        }

        [RoleAuthorize("User", "Admin")]
        [HttpPost]
        public ActionResult AddCard(DataBO Card)
        {
           UserBO user =  (UserBO)HttpContext.Session["User"];
            Card.AddedAt = DateTime.Now;
            Card.UserId = user.Id;
            Card.Username = user.Email;
            DataBL DataLogic = new DataBL();
            DataLogic.SubmitData(Card);


            return View();
        }

        [RoleAuthorize("User", "Admin")]
        public ActionResult Delete(int id)
        {
            DataBL DataLogic = new DataBL();
            DataLogic.DeleteData(id);




            return RedirectToAction(TempData["Action"].ToString(), "Data");
        }

        [RoleAuthorize("User", "Admin")]
        public ActionResult Edit(int id)
        {
            DataBL DataLogic = new DataBL();
            DataBO Card = DataLogic.GetDetails(id);
            DateTime ShortDate = DateTime.Parse(Card.PublishedYear.ToShortDateString());
            Card.PublishedYear = ShortDate;


            return View(Card);
        }

        [RoleAuthorize("User", "Admin")]
        [HttpPost]
        public ActionResult Edit(DataBO Card)
        {
            DataBL DataLogic = new DataBL();
            DataLogic.UpdateData(Card);



            return View();
        }

    }
}