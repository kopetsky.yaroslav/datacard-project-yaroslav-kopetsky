﻿using BO;
using BLL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Security.Claims;
using System.Security.Principal;
using System.Threading;
using PL.Models;

namespace PL.Controllers
{
    public class UserController : Controller
    {
        // GET: User
        [AllowAnonymous]
        public ActionResult Index()
        {
            return View();
        }


        [RoleAuthorize("Admin")]
        public ActionResult GetUsers()
        {
            UserBL UserBLogic = new UserBL();
            ViewBag.Data = UserBLogic.GetUserList();
            return View(ViewBag.Data);
        }

        [RoleAuthorize("Admin")]
        public ActionResult DeleteUser(int Id)
        {
            UserBL UserBLogic = new UserBL();
            ViewBag.Data = UserBLogic.DeleteUser(Id);
            return RedirectToRoute(new { controller = "User", action = "GetUsers" });
        }

        [AllowAnonymous]
        [HttpPost]
        public ActionResult Index(UserViewModel User)
        {

            if (!String.IsNullOrWhiteSpace(User.Password) && !String.IsNullOrWhiteSpace(User.ConfirmPassword))
            {
                UserBL UserBLogic = new UserBL();
                if (UserBLogic.IsValidEmail(User.Email))
                {
                    if (User.Password == User.ConfirmPassword)
                    {

                        UserBO NewUser = new UserBO() { Email = User.Email, Password = User.Password };

                        if (UserBLogic.AddUser(NewUser))
                        {
                            TempData["msg"] = "Account was successfully created!";
                        }
                        else
                        {
                            TempData["msg"] = "Email already registered!";
                        }
                    }
                    else
                    {
                        TempData["msg"] = "Passwords don`t match!";
                    }

                }
                else
                {
                    TempData["msg"] = "Email is not Valid";
                }

            }
            else
            {
                
                TempData["msg"] = "Some field is empty!";
            }
            return View();
        }

        [AllowAnonymous]
        public ActionResult TryLogin()
        {

            return View();
        }

        [AllowAnonymous]
        [HttpPost]
        public ActionResult TryLogin(UserBO User)
        {
            UserBL UserBLogic = new UserBL();
            if (UserBLogic.IsValidEmail(User.Email))
            {
                if (UserBLogic.LoginUser(User) != null)
                {

                    HttpContext.Session["User"] = User;
                    HttpContext.Session["Role"] = string.Join(",", User.Role);

                    return RedirectToRoute(new { controller = "Home", action = "Index" });
                }
                else
                {
                    TempData["msg"] = "Email or Password is Incorect!";
                    
                }
            }
            else
            {
                TempData["msg"] = "Email is Incorect!";
            }
            return View();
        }

        [AllowAnonymous]
        public ActionResult Logout()
        {
           
            if (HttpContext.Session["User"] != null)
            {
                HttpContext.Session["User"] = null;
                HttpContext.Session["Role"] = null;
                return RedirectToRoute(new { controller = "Home", action = "Index" });
            }

            return RedirectToRoute(new { controller = "Home", action = "Index" });
        }





    }
}
    
